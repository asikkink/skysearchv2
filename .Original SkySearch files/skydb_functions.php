<?php

function db_connect(){

	$db = mysqli_connect('localhost', 'asikkink', 'asikkink', 'stars');

	return $db; 
}

function get_radec_radians($db, $id, $tableName){

	$results = $db->query("SELECT $rarad, $decrad FROM $tableName WHERE id = '$id'");
	return $results;
}

function get_radec_decimal($db, $id, $tableName){

	$results = $db->query("SELECT $ra, $dec FROM $tableName WHERE id = '$id'");
	return $results;

}

function convert_radec_to_decimal($coordStr){
	
	//parse the strings into array
	$coord_arr = explode(" ", $coordStr);
	
	//retrieve the sign from dec
	$dec_d = preg_split("/([\+\-])/", $coord_arr[3], null, PREG_SPLIT_DELIM_CAPTURE);
	//$var = print_r($dec_d, true);
	//add up to decimal version
	$raDecimal = ($coord_arr[0] + ($coord_arr[1]/60) + ($coord_arr[2]/3600));
	$decDecimal = ($dec_d[2] + ($coord_arr[4]/60) + ($coord_arr[5]/3600));
	if($dec_d[1] == "-") $decDecimal *= -1;
	//return as array of two
	return array("ra" => 0.01 * (int)($raDecimal*100), "dec" => 0.01 * (int)($decDecimal*100)); 

}

function convert_radec_to_hmsdms($ra, $dec){

	//compute ra into hms
	$ra_h = (int)$ra;
	$ra_m = (int)(($ra - $ra_h)*60);
	$ra_s = ((($ra - $ra_h)*60) - $ra_m)*60;
	//compute dec into dms
	if($dec < 0 ) $sign = "-"; else $sign = "+";
	$dec_d = (int)abs($dec);
	$dec_m = (int)((abs($dec) - $dec_d)*60);
	$dec_s = (int)((((abs($dec) - $dec_d)*60)-$dec_m)*60);
	
	//make two separate strings
	$ra_hms = sprintf("%02d",$ra_h)."  ".sprintf("%02d",$ra_m)."  ".sprintf("%06.3f",$ra_s);
	$dec_dms = $sign.sprintf("%02d",$dec_d)."  ".sprintf("%02d",$dec_m)."  ".sprintf("%05.2f", $dec_s);
	//return as array of two
	return array( "ra" => $ra_hms, "dec" => $dec_dms);

}

function generateAirmassTable($ra, $dec, $lat, $lon, $date, $t_start, $t_end){

	
	$t_start = strtotime("8:00pm");
	$t_end = strtotime("5:00am tomorrow");
	
	$timeRemaining = 9; // need to eventually be able to calculate this based on the start and end times
	
	
	$ra = (double)$ra;
	$dec = (double)$dec;
	
	testFunc($lon);

	//initialize table string 
	$tableStr = "<table class ='table-bordered' style='width:100%'><tr><th>Local Time</th><th>UT</th><th>Local Sidereal Time</th><th>Hour Angle</th><th>Airmass</th></tr>";

	for($i = 0; $i <= $timeRemaining; $i++){
	
		$date = date("Y-m-d", $t_start);
		$data = getAirmassLocal($ra, $dec, $lat, $lon, $date, $t_start);
		
		$lst_h = (int)$data['lst'];
		$lst_m = ($data['lst'] - $lst_h)*60;
		$lst = sprintf("%02d:%02d",$lst_h,$lst_m);
		
		//error_log($data['ha']);
		$ha_h = (int)$data['ha'];
		$ha_m = (int)(($data['ha'] - $ha_h)*60);
		$ha = sprintf("%02d:%02d",$ha_h,$ha_m);
		
		//add new data to table string
		$tableStr .= "<tr><td>".date("H:i", $t_start)."</td><td>{$data['utc']}</td><td>$lst</td><td>$ha</td><td>{$data['am']}</td></tr>";
		
		//increment the time by an hour
		$t_start = strtotime('+60 minutes', $t_start);
	
		
	}
	
	$tableStr .= "</table>";

	return $tableStr;

}

function getAirmassLocal($ra, $dec, $lat, $lon, $date, $time){

	$lstData = getLstNew($date,$time,$lon);
	
	$amData = getAirmassForThisLST($ra, $dec, deg2rad($lat), $lstData['lst']);
	$data = array("am" => $amData["am"], "lst" => $lstData['lst'], "ha" => $amData["ha"], "utc" => $lstData["utc"]);

	return $data; 
	

}

//tests lst calculations for each minute from 2am to 3am
function testFunc($lon){

$t_start = strtotime("tomorrow 2:00am");

	for($i=0; $i < 61; $i++){

		$data = getLstNew(date("Y-m-d", $t_start), $t_start, $lon);
		echo "LST for ".date("Y-m-d H:i", $t_start).": ".$data['lst']."<br>";
		
		$t_start = strtotime("+1 minute", $t_start);

	}


}



// PHP version of the getLst function written by Alejandro Carvallo in Python 
// Calculates the lst for a given date, time, and longitude
// Reference equations can be found at: http://www.jgiesen.de/elevazmoon/basics/meeus.htm
function getLstNew($date,$time,$lon){
	//make timeStr out of date and time params
	$time = date("H:i:s", $time);
	$timeStr = $date." ".$time;
	//error_log("TimeStr: ".$timeStr);

	//convert HST to UTC
	$hawaiiDateTime = new DateTime($timeStr, new DateTimeZone('HST'));
	$datetime_utc = $hawaiiDateTime->setTimeZone(new DateTimeZone('UTC'));
	
	$utc_date = $datetime_utc->format('Y-m-d');
	$utc_time = $datetime_utc->format('H:i:s');
	
	$time_arr = explode(":", $utc_time);
	$date_arr = explode("-", $utc_date);
	
	$year = $date_arr[0];
	$month = $date_arr[1];
	$day = $date_arr[2];
	
	
	$a = floor((14 - $month)/12);
	$y = $year + 4800 - $a;
	$m = $month + 12*$a -3;
	
	$JDN = $day +  floor((153.0*$m+2.0)/5.0) + 365.0*$y + floor($y/4.0) - floor($y/100.0)+ floor($y/400.0) - 32045.0;
	
	$JD = $JDN + ($time_arr[0]-12.0)/24.0 + $time_arr[1]/1440.0 + $time_arr[2]/86400.0;

	$tj = ($JD - 2451545.0)/36525.0;
	
	$theta = 280.46061837 + 360.98564736629*($JD-2451545.0) + 0.000387933*pow($tj,2)- pow($tj,3)/38710000.0;

	$lmst = (($theta + $lon)%360)/15;

	return array("lst" => $lmst, "utc" => $utc_time);
}



function getAirmassForThisLST($ra, $dec, $lat, $lst){
	
	$ha = $lst - $ra;
	//error_log($ha);
	if($ha < -11) $ha %= 24;
	$am = getAirmass($ha, $lat, $dec);

	$data = array("am" => $am, "ha" => $ha);
	return $data;
}

function getAirmass($ha, $lat, $dec){
	
	$zd = getZD($ha, $lat, $dec);
	$am = 1/cos($zd);

	return $am;
}

function getZD($ha, $lat, $dec){

	//need to calculate this one out using the same formula used in alt/az coords hw
	$s_alt = sin(deg2rad($dec)) * sin(deg2rad($lat)) + cos(deg2rad($dec)) * cos(deg2rad($lat)) * cos(($ha) * (pi() / 12.0));
	
	$alt = asin($s_alt);
	//then 90 - the alt to get zd
	$zd = (pi()/2) - $alt;
	return $zd;
}

function generateDataList($type, $data = array(), $db){

	if($type == "star"){
	
		$results = $db->query("SELECT const_full from const where const_abbr = '{$data['con']}'");
	
		$const = $results->fetch_assoc();
	
		$hmsdms = convert_radec_to_hmsdms($data['ra'], $data['dec']);
		$str = "<div class='panel-body'><b>Starname:</b> {$data['proper']}<br>
			<b>RA: </b> {$hmsdms['ra']}<br>
			<b>Dec: </b> {$hmsdms['dec']}<br>
			<b>Constellation:</b> {$const['const_full']}<br>
			<b>Distance (pc):</b> {$data['dist']}<br>
			<b>Apparent Magnitude:</b> {$data['mag']}<br>
			<b>Absolute Magnitude:</b> {$data['absmag']}<br>
			<b>Luminosity:</b> {$data['lum']}<br>
			<b>Spectrum:</b> {$data['spect']}<br>
			<b>Color Index:</b> {$data['ci']}<br>
			</div>";

	}
	else if($type == "dso"){
		$results = $db->query("SELECT const_full from const where const_abbr = '{$data['const']}'");
	
		$const = $results->fetch_assoc();
		
	
		$hmsdms = convert_radec_to_hmsdms($data['ra'], $data['dec']);
		$str = "<div class='panel-body'><b>Catalog Name:</b> {$data['cat1']} {$data['id1']}<br>";
		if($data['type'] != ""){
			$results = $db->query("SELECT obj_desc from obj where obj_abbr = '{$data['type']}'");
		
			$type = $results->fetch_assoc();
			$str .= "<b>Object Type:</b> {$type['obj_desc']}<br>";
		}
		if($data['name'] != "") $str .= "<b> Common Name: </b>{$data['name']}<br>";
		$str .="
			<b>RA: </b> {$hmsdms['ra']}<br>
			<b>Dec: </b> {$hmsdms['dec']}<br>
			<b>Constellation:</b> {$const['const_full']}<br>";
		if($data['mag'] != 0) $str.= "<b>Apparent Magnitude:</b> {$data['mag']}<br>";
		if($data['angle'] != 0) $str.="<b> Position Angle:</b> {$data['angle']}<br>";
			
		$str.= "</div>";


	}
	return $str;
}


?>