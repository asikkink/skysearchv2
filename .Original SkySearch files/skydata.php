<!DOCTYPE html>
<html>
<head><title> Sky Search - Results </title>
<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<!-- include Aladin Lite CSS file in the head section of your page -->
<link rel="stylesheet" href="http://aladin.u-strasbg.fr/AladinLite/api/v2/latest/aladin.min.css" />
 
<!-- you can skip the following line if your page already integrates the jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js" charset="utf-8"></script>
</head>
<body style="background:url('background.jpg') no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

<div class="container-fluid">
<div class="page-header" style="color:white;"><div class="row">
	<div class="col-md-6"><h1><b>SkySearch!</b></h1></div><div class="col-md-6 text-right"><h1><b><a href='skysearch.html'>New Search</a></b></h1></div></div></div>

<?php
require_once('skydb_functions.php');

date_default_timezone_set('HST');
$date = date("Y-m-d");


$db = db_connect();
if(!$db) die("Could not connect to Star Database. Please try again.");

//set up query by first checking if the object is a star, then a dso
if($_POST['name']){

	$target = $_POST['name'];
	$sql = "SELECT * FROM `hygdata` WHERE `proper` = '$target'";	

	$result = $db->query($sql);

		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$target = $row['proper'];
		
			echo "<div class='alert alert-success'><strong>Match Found!</strong></div>";
			$dataList = generateDataList('star', $row , $db);
			echo "<div class='row'><div class='col-md-6 left-side'><div class= 'panel panel-primary'><div class='panel-heading'><b>Star Data</b></div>$dataList</div>";

			$amTable = generateAirmassTable($row['ra'], $row['dec'], 19.8207, -155.4681, $date, 20, 5);
			echo "<div class='row'><div class='col-md-6' style='width:100%'><div class='panel panel-primary'><div class='panel-heading'><b>Mauna Kea Airmass Table for Tonight ($date)</b></div><div class='panel-body'>$amTable</div></div></div></div>";
			
	}
		else{
			$targets = preg_split("/([a-zA-Z]+)/", $target, null, PREG_SPLIT_DELIM_CAPTURE);

			$sql = "SELECT * FROM `dso` WHERE `cat1` = '{$targets[1]}' AND `id1` = '{$targets[2]}'";	

			$result = $db->query($sql);

			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$target = "{$row['cat1']}{$row['id1']}";
				
				echo "<div class='alert alert-success'><strong>Match Found!</strong></div>";
				$dataList = generateDataList('dso', $row, $db);
				echo "<div class='row'><div class='col-md-6 left-side'><div class= 'panel panel-primary'><div class='panel-heading'><b>Deep Space Object Data</b></div>$dataList</div>";
				$amTable = generateAirmassTable($row['ra'], $row['dec'], 19.8207, -155.4681, $date, 20, 5);
				echo "<div class='row'><div class='col-md-6' style='width:100%'><div class='panel panel-primary'><div class='panel-heading'><b>Mauna Kea Airmass Table for Tonight ($date)</b></div><div class='panel-body'>$amTable</div></div></div></div>";
			}
			else echo "<div class='alert alert-danger'><strong>No match for $target found in the database. Please try again.</strong></div>";
		}


		$db->close();

		
} 
else if($_POST['coords']){

$coord_arr = convert_radec_to_decimal($_POST['coords']);

echo "<b style='color:white'>Searching for object at {$_POST['coords']} (RA: {$coord_arr['ra']} and Dec: {$coord_arr['dec']})</b><br><br>";

$sql = "SELECT * FROM `hygdata` WHERE `ra` LIKE '{$coord_arr['ra']}%' AND `dec` LIKE '{$coord_arr['dec']}%'";	

	$result = $db->query($sql);

		if ($result->num_rows > 0) {
			echo "<div class='alert alert-success'><strong>Match Found!</strong></div>";
			
		    $row = $result->fetch_assoc();
			$target = $row["proper"];
			$dataList = generateDataList('star', $row, $db);
			echo "<div class='row'><div class='col-md-6 left-side'><div class= 'panel panel-primary'><div class='panel-heading'><b>Star Data</b></div>$dataList</div>";
			$amTable = generateAirmassTable($row['ra'], $row['dec'], 19.8207, -155.4681, $date, 20, 5);
			echo "<div class='row'><div class='col-md-6' style='width:100%'><div class='panel panel-primary'><div class='panel-heading'><b>Mauna Kea Airmass Table for Tonight ($date)</b></div><div class='panel-body'>$amTable</div></div></div></div>";
	}
		else{

			$sql = "SELECT * FROM `dso` WHERE `ra` LIKE '{$coord_arr['ra']}%' AND `dec` LIKE '{$coord_arr['dec']}%'";	

			$result = $db->query($sql);

			if ($result->num_rows > 0) {
				echo "<div class='alert alert-success'><strong>Match Found!</strong></div>";
				
				$row = $result->fetch_assoc();
				$target = $row["cat1"].$row["id1"];
				
				$dataList = generateDataList('dso', $result->fetch_assoc(), $db);
				echo "<div class='row'><div class='col-md-6 left-side'><div class= 'panel panel-primary'><div class='panel-heading'><b>Deep Space Object Data</b></div>$dataList</div>";
				
				$amTable = generateAirmassTable($row['ra'], $row['dec'], 19.8207, -155.4681, $date, 20, 5);
				echo "<div class='row'><div class='col-md-6' style='width:100%'><div class='panel panel-primary'><div class='panel-heading'><b>Mauna Kea Airmass Table for Tonight ($date)</b></div><div class='panel-body'>$amTable</div></div></div></div>";
				
			}
			else echo "<div class='alert alert-danger'><strong>No match for coordinates {$_POST['coords']} found in the database. Please try again.</strong></div>"; 
		}


		$db->close();


}
else echo "<div class='alert alert-danger'><strong>No object name or coordinates were given. Please try again.</strong></div>"; 

if($dataList){


	$aladinPlugin = "<!-- insert this snippet where you want Aladin Lite viewer to appear and after the loading of jQuery -->
</div><div class='col-md-6 right-side'><div class='panel panel-info'><div class='panel-heading'><b>DSS image of $target:</b></div><div class='panel-body'><div id='aladin-lite-div' style='width:100%;height:500px;'></div></div>
<script type='text/javascript' src='http://aladin.u-strasbg.fr/AladinLite/api/v2/latest/aladin.min.js' charset='utf-8'></script>
<script type='text/javascript'>
    var aladin = A.aladin('#aladin-lite-div', {survey: 'P/DSS2/color', fov:4, target: '$target'});
</script>";

	echo $aladinPlugin."</div>";
}




?>
</div>
</body>
<footer style="color:white;"><center>Developed by Anna Sikkink and Alejandro Carvallo, Summer 2015.<center></footer>
</html> 

