
if (Meteor.isServer) {

	DsoData = new Mongo.Collection("dso");
	HygData = new Mongo.Collection("hygdata");
	ObjData = new Mongo.Collection("obj");
	ConstData = new Mongo.Collection("const");
	UserData = new Mongo.Collection("myusers");
	SavedSearches = new Mongo.Collection("searches");

	Meteor.methods({
			searchDSO: function(cat1, id1){
					console.log(cat1+id1);
					return DsoData.findOne({"$and": [{"cat1": 'M'},{"id1":31}]});
			},
			searchHygdata: function(objname){
					return HygData.findOne({"proper": objname});
			},
			searchObj: function(obj){
					return ObjData.findOne({"obj_abbr":obj});
			},
			searchConst: function(constel){
					return ConstData.findOne({"const_abbr":constel});
			},
			searchUsers: function(user, pass){
				return UserData.findOne({"username":user, "password":pass})
				
			},
			addUser: function(user, pass){
				UserData.insert({"username":user, "password":pass});
				
			},
			getUserSearches: function(user){
				return SavedSearches.findOne({"user":user});
				
			},
			addUserSearch:function(user, obj){
				SavedSearches.insert({"user":user, "search":obj});
			}
		});
}

if (Meteor.isClient) {
	
	Session.setDefault("curr_user",null);

target = {
	name: 'Vega',
	type: 'star',
	dep: new Deps.Dependency,
	getName: function(){
		this.dep.depend();
		return this.name;
	},
	setName: function(newTarget){
		if(newTarget !== this.name){
			this.name = newTarget;
			this.dep.changed();
		}
		
		return this.name;
	},
	getType: function(){
		this.dep.depend();
		return this.type;
	},
	setType: function(newTarget){
		if(newTarget !== this.name){
			this.name = newTarget;
			this.dep.changed();
		}
		
		return this.name;
	}
};



Template.results.helpers({
	type: function () {
      return target.getType();
    },
	isStar: function (typeObj){
		if(typeObj === "star") return true;
		else return false;
	},
	isDso: function(typeObj){
		if(typeObj === "dso") return true;
		else return false;
	},
	results:function(){
		return true;
	}
});

Template.results.events({
	"click #save_button": function(event){
		
		Meteor.call('addUserSearch', Session.get('curr_user'), event.target.value, function(error, result){
			if(error){
				console.log("Error on adding user's search to db.");
			}
			else{
				$('#save_button').html("Saved Successfully!");
				$('#save_button').prop("disabled", true);
				
				var newTable = updateSearchTable(Session.get('curr_user'));
				$('#searchesTable').html(newTable);
				
			}
		});
		
	}
});


Template.results.rendered = function(){	
	console.log(target.getName());

};


  Template.body.events({
	  "submit #searchform": function(event){
		  
		  //prevent default browser form submit
		  event.preventDefault();
		  
		  var name = event.target.name.value;
			
		if(!name){
			var coords = event.target.coords.value;
			//search on the coords, have to convert them
			//STUB: TODO LONG TERM
		}
		else{
			var re = /^[A-Za-z]+$/;
			if(re.test(name)){
			//force the name to be upper case
			name = name.charAt(0).toUpperCase() + name.slice(1);
			//get the database results by searching on the name given
			//see if it is in the hygdata catalog first
			Meteor.call('searchHygdata', name, function(error, result){
				if(error){
					//handle error
					console.log("error!");
				}
				else{
						target.setName(name);
						target.setType = "star";
						
			
						
						
						var tableStr = generateAirmassTable(result.ra, result.dec);
						
						var convertedvals = convert_radec_to_hmsdms(result.ra, result.dec);
						
						var con = result.con;
						//get matching constellation name
						Meteor.call('searchConst', result.con, function(error, result2){
							if(error){
								console.log("Error!");
							}
							else{
								con = result2.const_full;
							}
						});
						
						//set up return string
						$('#dataPanelBody').html("<b>Starname:</b> "+result.proper+"<br><b>RA: </b> "+convertedvals.ra+"<br><b>Dec: </b> "+convertedvals.dec+"<br><b>Constellation: </b>"+con+"<br><b>Distance (pc): </b>"+result.dist+"<br><b>Apparent Magnitude: </b>"+result.mag+"<br><b>Absolute Magnitude: </b>"+result.absmag+"<br><b>Luminosity: </b> "+result.lum+"<br><b>Spectrum: </b>"+result.spect+"<br><b>Color Index: </b>"+result.ci+"<br>");
						
						var date = new Date();
						
						$('#status').html('<b>Match Found!</b>');
						$('#date').html("<b>Mauna Kea Airmass Table for Tonight ("+(date.getMonth()+1)+"-"+date.getDate()+"-"+date.getFullYear()+")</b>");
						$('#targetName').html("<b>DSS image of "+name+"</b>");
						$('#amtable').html(tableStr);
						
						
						
						}
					});
				}
				else{
							//else check the dso catalog
							
							//split the name string by letters and numbers, then capitalize the letters
							var name_arr = name.split(" ");
							name_arr[0] = name_arr[0].toUpperCase();
							
							console.log(name_arr[0]+name_arr[1]);
							
							Meteor.call('searchDSO', name_arr[0], name_arr[1], function(error, result){
								if(error){
									console.log("error!");
								}
								else{
									
										console.log(result);
										target.setName(name_arr[0]+name_arr[1]);
										target.setType("dso");
										var tableStr = generateAirmassTable(result.ra, result.dec);
										
										
										
										var con = result.constel;
										//console.log(result.constel);
										Meteor.call('searchConst', result.constel, function(error, result2){
											if(error){
												console.log("error getting dso const");
											}
											else{
												con = result2.const_full;
											}
										});
										
										
										
										var htmlStr = "<b>Catalog Name:</b> "+name;
										
										
										if(result.type !== ""){
											
											var type = result.type;
											//search for the corresponding type
											Meteor.call('searchObj', result.type, function(error, result2){
												if(error){
													console.log('error on grabbing obj type');
												}
												else{
													type = result2.obj_desc;
												}
											});
											
											htmlStr = htmlStr+"<br><b>Object Type:</b> "+type;
										}
										
										if(result.name !== ""){
											htmlStr = htmlStr+"<br><b>Common Name:</b> "+result.name;
										}
										
										var convertedvals = convert_radec_to_hmsdms(result.ra, result.dec);
						
										
										htmlStr = htmlStr+"<br><b>RA: </b> "+convertedvals.ra+"<br><b>Dec: </b> "+convertedvals.dec+"<br><b>Constellation: </b>"+con;
										
										if(result.mag != 0){
											htmlStr = htmlStr+"<br><b>Apparent Magnitude: </b>"+result.mag;
										}
										
										if(result.angle != 0){
											htmlStr = htmlStr+"<br><b>Position Angle: </b>"+result.angle;
										}
										
										$('#dataPanelBody').html(htmlStr+"<br>");
										
										
										var date = new Date();
										
										$('#status').html('Match Found!');
										$('#date').html("<b>Mauna Kea Airmass Table for Tonight ("+(date.getMonth()+1)+" "+date.getDate()+" "+date.getFullYear()+")</b>");
										$('#targetName').html("<b>DSS image of "+name+"</b>");
										$('#amtable').html(tableStr);
									
								}
							});
			
							
							
						}
				}
				
			
		
	  
			$('#save_button').val(name);
		
			$('#aladin-lite-div').html("<script>var aladin = A.aladin('#aladin-lite-div', {survey: 'P/DSS2/color', fov:4, target: '"+name+"'});</script>");
		
		//switch to the results page
		var divToHide = document.getElementsByClassName("page");
        for(i = 0; i < divToHide.length; i++)
        {
            divToHide[i].style.display = 'none';
        }
		divToShow = document.getElementById("searchresults");
        divToShow.style.display = 'block';
		
		
		

		//clear the form
		event.target.name.value = "";
		event.target.coords.value = "";
		
		  
	  }
 });
 
 Template.body.events({
	"click .nav-bar":function (event){
		event.preventDefault();
		//hide all div with class=page
		hideDivByClassName("page");
		//show the div with href took out the #
		var divToShow = event.target.getAttribute("href");
		var divToShowText = divToShow.substring(1);
		divToShow = document.getElementById(divToShowText);
		divToShow.style.display = 'block';
		//if user clicked log out 
		if(divToShowText == "logout")
		{
			Session.set('curr_user', null);
			$('#savesearch_button').html('');
			//hide the My Search tab on nav bar
			var mySearchOnNavBar = document.getElementById("savedsearches_nav");
			mySearchOnNavBar.style.display = 'none';
			//change the log out button in to log in
			var logLink = document.getElementById("log");
			logLink.innerHTML = "Log In";
			logLink.setAttribute("href", "#login");
		}
	}
	
  });
  
   Template.body.events({
	"submit #logInForm":function (event){
		event.preventDefault();
		var username = event.target.username.value;
		var password = event.target.password.value;
		//if pasword is correct
		Meteor.call('searchUsers', event.target.username.value, event.target.password.value, function(error, result){
			if(error){
				console.log("error logging in");
				$('#loginerror').html("<div class='alert alert-danger'>Either you entered an incorrect username or password, or you have not signed up for this site yet. Please try again, or register with the sign up button.</div>");
				event.target.username.value ="";
				event.target.password.value ="";
				
			}
			else{
				if(!result){
					$('#loginerror').html("<div class='alert alert-danger'>Either you entered an incorrect username or password, or you have not signed up for this site yet. Please try again, or register with the sign up button.</div>");
				event.target.username.value ="";
				event.target.password.value ="";
				}
				else{
					Session.set("curr_user", event.target.username.value);
					
					//find this user's searches
					var st = updateSearchTable(event.target.username.value);
					$('#searchesTable').html(st);
					
						
					$('#savesearch_button').html('<button id="save_button" class="btn btn-lg btn-success" style="float:right">Save Search</button>');
					
					//display My Searches tab on nav bar
					var mySearchOnNavBar = document.getElementById("savedsearches_nav");
					mySearchOnNavBar.style.display = 'block';
					//change the log in button into log in as username
					var logLink = document.getElementById("log");
					logLink.innerHTML = "Log Out";
					logLink.setAttribute("href", "#logout");
					//go back to home page
					hideDivByClassName("page");
					var divToShow = document.getElementById("home");
					divToShow.style.display = 'block';
				}
				
			}
		});	
		
	}
  });
  
  Template.body.events({
    "click #signup_button":function (event){
		
		event.preventDefault();
		hideDivByClassName("page");
		var divToShow = document.getElementById("signup");
		divToShow.style.display = 'block';
	}
  });
  
  Template.body.events({
    "submit #SignUpForm":function (event){
		
		
		Meteor.call('addUser', event.target.username.value, event.target.password.value, function(error, results){
			if(error){
				console.log("Error adding user.");
			}
			else{
				
				$('#searchesTable').html("<div class='alert alert-info'><b>You have no searches saved. If you save some of your searches, they will be displayed here for future reference.</b></div>");
				
				$('#savesearch_button').html('<button id="save_button" class="btn btn-lg btn-success" style="float:right">Save Search</button>');
				
				Session.set("curr_user", event.target.username.value);
				var mySearchOnNavBar = document.getElementById("savedsearches_nav");
				mySearchOnNavBar.style.display = 'block';
				//change the log in button into log in as username
				var logLink = document.getElementById("log");
				logLink.innerHTML = "log out";
				logLink.setAttribute("href", "#logout");
			}
		});
		
		event.preventDefault();
		hideDivByClassName("page");
		var divToShow = document.getElementById("signupconf");
		divToShow.style.display = 'block';
	}
  });
  
function updateSearchTable(user){
	
	var table = "<table class='table table-striped'><tr><th>Searched Object</th><th>Date Searched</th></tr><tr><td>Vindemiatrix</td><td>12-1-15</td></tr><tr><td>Sirius</td><td>12-1-15</td></tr><tr><td>Rigel</td><td>12-2-15</td></tr>";
	
	Meteor.call('getUserSearches', user, function(error, result){
		if(error){
			console.log('error retrieving searches.');
			table = "<div class='alert alert-danger><b>An error occurred, unable to retrieve saved searches. Please try again.</b></div>";
		}
		else{
			if(!result){
				table = "<div class='alert alert-warning><b>You have no saved searches. Press the save button after running a search to save it.</b></div>";
			}
			else{
				console.log(result);
				//generate table
				
				table = table+"<tr><td>"+result.search+"</td><td>test</td></tr>";
				console.log("Table added");
			}
		}
	});
	table = table+"</table>";
	console.log(table);
	return table;
	}

 
}
  
  
function hideDivByClassName(className)
{
	var divToHide = document.getElementsByClassName(className);
	for(i = 0; i < divToHide.length; i++)
	{
		divToHide[i].style.display = 'none';
	}
}



/////////////////////////////////
//AIRMASS CALCULATION FUNCTIONS//
/////////////////////////////////
function convert_radec_to_hmsdms(ra, dec){

  //compute ra into hms
  var ra_h = Math.floor(ra);
  var ra_m = Math.floor((ra - ra_h)*60);
  var ra_s = (((ra - ra_h)*60) - ra_m)*60;
  //compute dec into dms
  if(dec < 0 ) var sign = "-"; else  var sign = "+";
  var dec_d = Math.floor(Math.abs(dec));
  var dec_m = Math.floor((Math.abs(dec) - dec_d)*60);
  var dec_s = Math.floor((((Math.abs(dec) - dec_d)*60)-dec_m)*60);

  //make two separate strings
  var ra_hms = ra_h+"  "+Math.floor(ra_m)+"  "+ra_s.toFixed(3);
  var dec_dms = sign+dec_d+"  "+Math.floor(dec_m)+"  "+dec_s.toFixed(2);
  //return as json
  var hmsdms = '{ "ra": "'+ra_hms+'", "dec": "'+dec_dms+'"}';


  return JSON.parse(hmsdms);
}

///////////////////////////////////////////////////
//BASED ON PYTHON FUNCTIONS BY ALEJANDRO CARVALLO//
///////////////////////////////////////////////////
function generateAirmassTable(ra, dec){

	
	var date = new Date(); 
	
	var dateStr = (date.getMonth()+1)+"-"+date.getDate()+"-"+date.getFullYear();
	
	//8pm
	var localStartTime = 20;
	
	//need to convert to UTC for calculations
	var utcTime = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
	
	var day = utcTime.getDate();
	var month = utcTime.getMonth()+1;
	var year = utcTime.getFullYear();
	
	//6 UTC is 20:00 hawaii time
	var t_start = new Date(year, month, day, 6, 0, 0);
	
	//this does not handle daylight savings or new month/year senarios. 16 UTC is 6am hawaii time
	var t_end = new Date(year, month, day+1, 16, 0, 0);
	
	//also, lat and lon are hardcoded, eventually these should be changable values
	var lat = 19.8261;
	var lon = -155.4681;
	
	//then convert lat to radians using x*pi/180.0
	var rad_lat = lat*(Math.PI/180.0);
	//also convert dec to radians
	var rad_dec = dec*(Math.PI/180.0);
	
	
	
	//now we can build the table!!!!
	var tableStr = "<table class ='table table-condensed' style='width:100%'><tr class='info'><th>Local Time</th><th>UT</th><th>Local Sidereal Time</th><th>Hour Angle</th><th>Airmass</th></tr>";
	
	
	//this for loop is very hardcoded, needs to be more general...
	for(i = 0; i <= (t_end.getHours() - t_start.getHours()); i++){
		var t = (i+t_start.getHours())%24;
		var lt = (i+localStartTime)%24;
		var values = getAirmassLocal(ra, rad_dec, rad_lat, lon, date, t);
		//format returns into the table (and possibly the graph too)
		var localtime = lt+":00";
		var utctime = t+":00";
		
		var formattedLsthours = Math.floor(values.lst);
		var formattedLst = formattedLsthours+":"+Math.floor((values.lst - formattedLsthours)*60.0);
		
		if(values.ha < 0){
			var formattedha = -1*(Math.floor(values.ha*-1));
		}
		else var formattedha = Math.floor(values.ha);
		
		var formattedam = (values.airmass).toFixed(5);
		
		if(values.airmass > 0){
			tableStr = tableStr+'<tr class="success">';
		}
		else tableStr = tableStr+"<tr>";
	
		tableStr = tableStr+"<td>"+localtime+"</td><td>"+utctime+"</td><td>"+formattedLst+"</td><td>"+formattedha+"</td><td>"+formattedam+"</td></tr>";
	
	}
	
	tableStr = tableStr+"</table>";
	
	
	return tableStr;
}

function getAirmassLocal(ra, dec, lat, lon, date, t){
	var lst = getLst(date, t, lon);
	var values = getAirmassForThisLst(ra,dec,lat,lst);
	
	return JSON.parse('{"airmass": '+values.am+', "lst": '+lst+', "ha": '+values.ha+'}');
}

function getAirmassForThisLst(ra, dec, lat, lst){
	var ha = lst - ra;
	if (ha < -11){
		ha = ha+24;
	}
	var am = getAirmass(ha, lat, dec);
	
	return JSON.parse('{"am": '+am+', "ha": '+ha+'}');
}

function getLst(date, t, lon){
	var day = date.getDate();
	var month = date.getMonth()+1; //JS starts counting months at 0...
	var year = date.getFullYear();
	
	var a = Math.floor((14 - month)/12);
	var y = year + 4800 - a;
	var m = month + 12 * a - 3;
	
	var JDN = day + Math.floor((153.0*m+2.0)/5.0) + 365.0*y + Math.floor(y/4.0) - Math.floor(y/100.0) + Math.floor(y/400.0) - 32045.0;

	var JD = JDN + (t-12.0)/24.0; // + min/1440.0 + sec/86400.0
	
	var tj = (JD - 2451545.0)/36525.0;
	
	var theta = 280.46061837 + 360.98564736629*(JD - 2451545.0) + 0.000387933*(Math.pow(tj, 2)) - (Math.pow(tj, 3))/38710000.0;
	
	var lmst = ((theta + lon)%360)/15;
	
	return lmst;
	
}

function getAirmass(ha, lat, dec){
	var zd = getZD(ha, lat, dec);
	var airmass = 1/Math.cos(zd*(Math.PI/180.0));
	
	return airmass;
}

function getZD(ha, lat, dec){
	var rad_ha = ha*(15.0)*(Math.PI/180.0)*Math.cos(dec);
	var zd = 90.0 - Math.asin(Math.sin(lat)*Math.sin(dec)+Math.cos(lat)*Math.cos(dec)*Math.cos(rad_ha)) * (180.0/Math.PI);
	var zdRad = zd*(Math.PI/180.0);
	
	return zd;
}

